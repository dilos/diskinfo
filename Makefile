#
# This file and its contents are supplied under the terms of the
# Common Development and Distribution License ("CDDL"), version 1.0.
# You may only use this file in accordance with the terms of version
# 1.0 of the CDDL.
#
# A full copy of the text of the CDDL should have accompanied this
# source.  A copy of the CDDL is also available via the Internet at
# http://www.illumos.org/license/CDDL.
#

#
# Copyright 2015 Igor Kozhukhov <ikozhukhov@gmail.com>
# Copyright 2015 Joyent
#

MACH := $(shell uname -p)
MACH32_1 = $(MACH:sparc=sparcv7)
MACH32 = $(MACH32_1:i386=i86)
MACH64_1 = $(MACH:sparc=sparcv9)
MACH64 = $(MACH64_1:i386=amd64)

PROG = diskinfo

OBJS = diskinfo.o

CC = /usr/gcc/4.8/bin/gcc

CFLAGS += -O2 -m64
#CFLAGS += -O2 -m32

LDFLAGS = -L/usr/lib/fm/$(MACH64) -R/usr/lib/fm/$(MACH64)
#LDFLAGS = -L/usr/lib/fm -R/usr/lib/fm
LDFLAGS += -ldiskmgt -lnvpair -ltopo

all: $(PROG)

$(PROG): $(OBJS)
	$(CC) $(CFLAGS) $(OBJS) -o $(PROG) $(LDFLAGS)

clean:
	rm -f $(OBJS) 
	rm -f $(PROG)
